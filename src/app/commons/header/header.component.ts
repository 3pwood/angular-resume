import { Component, OnInit } from '@angular/core';
import { MenuItem } from 'primeng/api';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

    constructor( ) { }

    items: MenuItem[];
    iconItems: MenuItem[];

    ngOnInit() {
        this.items = [
            { icon: 'fa-home', routerLink: '/home'},
            { label: 'Compétences', routerLink: '/competences' },
            { label: 'Expériences', routerLink: '/experiences' },
            { label: 'Formations', routerLink: '/formations' },
            { label: 'Loisirs', routerLink: '/loisirs' }
        ];
        this.iconItems = [
            { icon: 'fa-home', routerLink: '/home'},
            { icon: 'fa-star', routerLink: '/competences' },
            { icon: 'fa-flask', routerLink: '/experiences' },
            { icon: 'fa-graduation-cap', routerLink: '/formations' },
            { icon: 'fa-gamepad', routerLink: '/loisirs' }
        ];
    }

}
