import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SkillListComponent } from './skill-list/skill-list.component';

import { TableModule } from 'primeng/table';
import { PanelModule } from 'primeng/panel';
import { RatingModule } from 'primeng/rating';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        TableModule,
        PanelModule,
        RatingModule
    ],
    declarations: [SkillListComponent]
})
export class SkillsModule { }
