import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-skill-list',
    templateUrl: './skill-list.component.html',
    styleUrls: ['./skill-list.component.css']
})
export class SkillListComponent implements OnInit {

    skill: any[];
    bdd: any[];

    constructor() { }

    ngOnInit() {
        this.skill = [
            { name: 'Nodejs', rate: 7 },
            { name: 'JAVA', rate: 5 },
            { name: 'PHP', rate: 5 },
            { name: 'Javascript', rate: 9 },
            { name: 'Angular', rate: 8 },
            { name: 'HTML5', rate: 10 },
            { name: 'CSS3', rate: 6 },
            { name: 'SQL', rate: 8 },
            { name: 'Docker', rate: 8 },
            { name: 'Git', rate: 9 },
        ]

        this.bdd = [
            { name: 'SQL Server', rate: 7 },
            { name: 'MySQL', rate: 7 },
            { name: 'PostgreSQL', rate: 8 },
            { name: 'MongoDB', rate: 6 }
        ]
    }

}