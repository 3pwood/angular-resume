import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormationListComponent } from './formation-list/formation-list.component';
import { PanelModule } from 'primeng/panel';

@NgModule({
  imports: [
    CommonModule,
    PanelModule,
  ],
  declarations: [FormationListComponent]
})
export class FormationModule { }
