import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-formation-list',
    templateUrl: './formation-list.component.html',
    styleUrls: ['./formation-list.component.css']
})
export class FormationListComponent implements OnInit {

    formation: any[];

    constructor() { }

    ngOnInit() {
        this.formation = [
            {
                year: 2018,
                school: 'ENI Ecole Informatique (Rennes)',
                title: 'Concepteur Développeur Informatique'
            },
            {
                year: 2016,
                school: 'ENI Ecole Informatique (Rennes)',
                title: 'POEC - Développeur intégrateur web PHP'
            },
            {
                year: 2015,
                school: 'IESA Multimédia (Paris)',
                title: 'Mastère Chef de projet en stratégie digitale'
            },
            {
                year: 2013,
                school: 'ESTEI (Bordeaux)',
                title: 'Mastère 1 - Ingénieur systèmes embarqués & télécommunications'
            },
            {
                year: 2011,
                school: 'ESTEI (Bordeaux)',
                title: 'DEES Ingénieur des Sciences et Techniques'
            },
            {
                year: 2008,
                school: 'Lycée La Fayette (Champagne sur Seine)',
                title: 'BTS Systèmes embarqués'
            },
        ]
    }
}
