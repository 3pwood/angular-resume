import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
/* PRIMENG */
import { ButtonModule } from 'primeng/button';
import { MenubarModule } from 'primeng/menubar';
import { PanelModule } from 'primeng/panel';
import { CardModule } from 'primeng/card';
import { FieldsetModule } from 'primeng/fieldset';
import {DialogModule} from 'primeng/dialog';
/* MODULES*/
import { RoutingModule } from './routing.module';
import { PdfViewerModule } from 'ng2-pdf-viewer';
/* COMPONENTS */
import { AppComponent } from './app.component';
import { HeaderComponent } from './commons/header/header.component';
import { FooterComponent } from './commons/footer/footer.component';
import { HomeComponent } from './home/home.component';
import { SkillsModule } from './skills/skills.module';
import { FormationModule } from './formation/formation.module';
import { ExperienceModule } from './experience/experience.module';
import { LoisirModule } from './loisir/loisir.module';
import { LostPageComponent } from './commons/lost-page/lost-page.component';

@NgModule({
    declarations: [
        AppComponent,
        HeaderComponent,
        FooterComponent,
        HomeComponent,
        LostPageComponent
    ],
    imports: [
        HttpClientModule,
        RoutingModule,
        BrowserAnimationsModule,
        CommonModule,
        MenubarModule,
        ButtonModule,
        PanelModule,
        FieldsetModule,
        CardModule,
        SkillsModule,
        FormationModule,
        ExperienceModule,
        LoisirModule,
        PdfViewerModule,
        DialogModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
