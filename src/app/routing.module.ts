import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from './home/home.component';
import { SkillListComponent } from './skills/skill-list/skill-list.component';
import { ExperienceListComponent } from './experience/experience-list/experience-list.component';
import { FormationListComponent } from './formation/formation-list/formation-list.component';
import { LoisirListComponent } from './loisir/loisir-list/loisir-list.component';
import { LostPageComponent } from './commons/lost-page/lost-page.component';

const appRoutes: Routes = [
    { path: '', component: HomeComponent },
    { path: 'home', component: HomeComponent },
    { path: 'competences', component: SkillListComponent },
    { path: 'experiences', component: ExperienceListComponent },
    { path: 'formations', component: FormationListComponent },
    { path: 'loisirs', component: LoisirListComponent },
    { path: '**', component: LostPageComponent }
];

@NgModule({
    imports: [RouterModule.forRoot(appRoutes)],
    exports: [RouterModule]
})
export class RoutingModule { }
