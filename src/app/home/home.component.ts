import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

    display: Boolean;
    pdf: String;
    prez: String;
    contact: any;
    winWidth: Number;

    constructor() { }

    ngOnInit() {
        this.prez = `
        Développeur passionné de jeux vidéo et des nouvelles technologies,
        le développement est pour moi le rêve d'une vie. Je sais communiquer et travailler en équipe et 
        c'est pour moi un réel moteur que d'échanger des idées, de proposer des solutions et de résoudre des problèmes.
        Je cherche à monter en compétences tout en apportant mon savoir faire et mon expérience de développeur et de joueur
        pour offrir la meilleure expérience possible.
        `
        this.contact = {
            street: '2, Parcou Bris',
            city: '29340 Riec sur Belon',
            tel: '06.99.09.51.72',
            mail: 'p.castrec@gmail.com'
        }
    }

    showDialog() {
        this.display = true;
        this.winWidth = window.innerWidth;
        this.pdf = '/assets/cv_2018.pdf';
    }

}
