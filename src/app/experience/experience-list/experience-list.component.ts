import { Component, OnInit } from '@angular/core';

@Component({
    selector: 'app-experience-list',
    templateUrl: './experience-list.component.html',
    styleUrls: ['./experience-list.component.css']
})
export class ExperienceListComponent implements OnInit {

    experience: any[];

    constructor() { }

    ngOnInit() {
        this.experience = [
            {
                year: 2017,
                duration: '12 mois',
                society: 'Equantec (Brest)',
                project: `
                Développement d'un intranet et extranet métier (Javascript, Angularjs, Angular, Node, HTML5, CSS3).
                Base de données (PostgreSQL) en architecture micro service par containerization (Docker)
                `
            },
            {
                year: 2016,
                duration: '3 mois',
                society: 'ENI Ecole Informatique (Rennes)',
                project: "Réalisation d'un site de gestion de médias audio et vidéo (HTML5, CSS3, PHP, Javascript) \n Symfony : Mise en place d'un front et d'un back office sécurisé (Fos UserBundle)"
            },
            {
                year: 2012,
                duration: '3 mois',
                society: 'DGD Sytème (Bordeaux)',
                project: "Conception et simulation d'un système de triage de tubes pharmaceutiques"
            },
            {
                year: 2008,
                duration: '2 mois',
                society: 'ESTL (Warrington, Angleterre)',
                project: "Réalisation d'un système de contrôle de température dans une chambre à vide"
            },
        ]
    }

}
