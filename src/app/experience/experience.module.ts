import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ExperienceListComponent } from './experience-list/experience-list.component';
import { PanelModule } from 'primeng/panel';

@NgModule({
    imports: [
        CommonModule,
        PanelModule,
  ],
  declarations: [ExperienceListComponent]
})
export class ExperienceModule { }
