import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoisirListComponent } from './loisir-list/loisir-list.component';
import { PanelModule } from 'primeng/panel';
import { FormsModule } from '@angular/forms';

@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        PanelModule
    ],
    declarations: [LoisirListComponent]
})
export class LoisirModule { }
