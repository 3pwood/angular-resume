import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoisirListComponent } from './loisir-list.component';

describe('LoisirListComponent', () => {
  let component: LoisirListComponent;
  let fixture: ComponentFixture<LoisirListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoisirListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoisirListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
